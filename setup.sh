#!/bin/bash

pkg_meta () {
    case $1 in
        consul)
            VERSION=0.9.0

            case $2 in
                semv)
                    echo $VERSION
                    ;;
                link)
                    echo https://releases.hashicorp.com/consul/${VERSION}/consul_${VERSION}_linux_amd64.zip
                    ;;
            esac
            ;;
        nomad)
            VERSION=0.6.0

            case $2 in
                semv)
                    echo $VERSION
                    ;;
                link)
                    echo https://releases.hashicorp.com/nomad/${VERSION}/nomad_${VERSION}_linux_amd64.zip
                    ;;
            esac
            ;;
        vault)
            VERSION=0.7.3

            case $2 in
                semv)
                    echo $VERSION
                    ;;
                link)
                    echo https://releases.hashicorp.com/vault/${VERSION}/vault_${VERSION}_linux_amd64.zip
                    ;;
            esac
            ;;

        minio)
            VERSION=latest

            case $2 in
                link)
                    echo https://dl.minio.io/server/minio/release/linux-amd64/minio
                    ;;
            esac
            ;;
    esac
}

#cd /app/programs
cd ./programs

for alias in consul nomad vault ; do
    sources=$(pkg_meta $alias link)
    version=$(pkg_meta $alias semv)

    wget -c $sources

    case $1 in
        consul|nomad|vault)
            unzip ${alias}_${version}_linux_amd64.zip

            rm -f ${alias}_${version}_linux_amd64.zip
            ;;
    esac
done

